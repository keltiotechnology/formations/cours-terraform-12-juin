Gestion des ouvertures des flux

Contexte :
Archi      : donne les goods practices
DevOps : code le terraform infra
Sécurité : validations des ouvertures de flux
Support : doit gérer l’ouverture de flux


Solution :
un projet par infra par env
REPO 1				REPO 2
infra modulaire				réseaux security group




Identifier du refactoring qui va casser les ressources existantes

Contexte :
Service modifié par les clouds providers
Changement de la landing zone

Comment anticiper les changementes des clouds providers

Impact changement structure des données (map -> string) 


Migration smooth (canary deployment)
Choix des services et de l’infra qu’on fait



Profondeur gestion du tfstates




Check list cadrage projet :
Design thinking :

Définir les process et les règles de nommage, des acteurs …
		Acteurs :
			Lister les acteurs
			Définir pour chacun leurs responsabilités
			Vérifier si les acteurs peuvent intervenir dans l’organisation / les projets
		Convention :
			



Développeur / DevOps
Prendre connaissances de ton rôle dans l’organisation
Prendre connaissances des contraintes / standards (ligne de dev, nommages)
Voir l’existant (vérifier si un service dans l’entreprise fait déjà ce que je veux)
	Si ça existe déjà :
		Pourri : Remonte PO / PM
		Marche : Utilise
	Si y a pas :
		Remonter besoin : PM
		Notifie l’architecte pour un dev custom

Demander ou faire le DAT (dessiner l’infra, et déterminer les services à utiliser en fonction du besoin métier que)



Lire la doc terraform et determine les modules à développer
Développe les modules et les infras
Infracost (gestion des coûts)
bridgecrew (gestion de la sécu)
Tflint (Linting : Qualité du code et la maintenabilité)
terraform fmt (Linting : Qualité du code et la maintenabilité)
Pousse dans le référentiel
On test sur un ou deux env
Prod

