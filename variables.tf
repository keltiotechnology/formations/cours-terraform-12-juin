variable "project" {
  type        = string
  description = "TODO"
  default     = "keltio"
}

variable "env" {
  type        = string
  description = "TODO"
  default     = "dev"
}

variable "zone" {
  type        = string
  description = "La zone gcp dans laquelle les ressources seront déployées"
  default     = "us-central1-a"
}
