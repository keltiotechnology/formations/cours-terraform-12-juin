module "cluster_bigquery" {
  source = "./modules/cluster_bigquery"

  project           = "systeme-u"
  env               = "prod"

  domain            = "systeme-u.fr"
  table_source_uris = [
    "https://docs.google.com/spreadsheets/d/1fKQsjD--G9tvJQqR4f76-ZB43tQoYI2Nym34TcbzmOY"
  ]
}
