terraform {
  required_version = ">= 0.13.0"

  backend "remote" {
    organization = "Swappy"

    workspaces {
      name = "demo"
    }
  }
}
