locals {
  cluster_name            = "${var.project}-${var.env}-${var.name}"
  full_service_account_id = "${var.project}-${var.env}-${var.service_account_id}"
}


resource "google_service_account" "dataproc_service_account" {
  account_id   = local.full_service_account_id
  display_name = "Dataproc service Account"
}

data "google_iam_policy" "admin" {
  binding {
    role = "roles/dataproc.admin"

    members = [
      "serviceAccount:${google_service_account.dataproc_service_account.email}"
    ]
  }
}

resource "google_service_account_iam_policy" "admin_account_iam" {
  service_account_id = google_service_account.dataproc_service_account.name
  policy_data        = data.google_iam_policy.admin.policy_data
}

// resource "google_dataproc_cluster" "simplecluster" {
//   count  = 0

//   name   = local.cluster_name
//   region = "us-central1"

//   graceful_decommission_timeout = "120s"
//   labels = {
//     demo = "true"
//   }

//   cluster_config {
//     staging_bucket = "dataproc-staging-bucket"

//     master_config {
//       num_instances = 1
//       machine_type  = "e2-small"
//       disk_config {
//         boot_disk_type    = "pd-ssd"
//         boot_disk_size_gb = 15
//       }
//     }

//     worker_config {
//       # Worker must be at least 2
//       num_instances    = 2
//       machine_type     = "e2-small"
//       min_cpu_platform = "Intel Skylake"
//       disk_config {
//         boot_disk_size_gb = 30
//         num_local_ssds    = 1
//       }
//     }

//     preemptible_worker_config {
//       num_instances = 0
//     }

//     # Override or set some custom properties
//     software_config {
//       image_version = "1.3.7-deb9"
//       override_properties = {
//         "dataproc:dataproc.allow.zero.workers" = "true"
//       }
//     }

//     gce_cluster_config {
//       tags = ["foo", "bar"]
//       # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
//       service_account = google_service_account.dataproc_service_account.email
//       service_account_scopes = [
//         "cloud-platform"
//       ]
//     }

//     # You can define multiple initialization_action blocks
//     initialization_action {
//       script      = "gs://dataproc-initialization-actions/stackdriver/stackdriver.sh"
//       timeout_sec = 500
//     }
//   }

//   depends_on = [
//     google_service_account_iam_member.gce_default_account_iam
//   ]
// }
