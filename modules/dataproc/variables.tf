variable "project" {
  type = string
  description = "TODO"
  default = "keltio"
}

variable "env" {
  type = string
  description = "TODO"
  default = "dev"
}

variable "name" {
  type = string
  description = "The name of the dataproc cluster"
  default = "dataproc"
}

variable "service_account_id" {
  type = string
  description = "The name of the dataproc cluster"
  default = "dataproc"
}
