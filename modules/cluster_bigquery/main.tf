locals {
  dataset_id = "${replace(var.project, "-", "_")}_${var.env}_${var.id}"
  account_id = "${var.project}-${var.env}-${var.id}-svca"
  table_id   = "${var.project}-${var.env}-${var.table_id}"
}

resource "google_bigquery_dataset" "dataset" {
  dataset_id                  = local.dataset_id
  friendly_name               = local.dataset_id
  description                 = var.description
  location                    = var.location
  default_table_expiration_ms = var.default_table_expiration_ms
  delete_contents_on_destroy  = var.delete_contents_on_destroy

  labels = {
    project = var.project
    env     = var.env
  }

  access {
    role          = "OWNER"
    user_by_email = google_service_account.bqowner.email
  }

  access {
    role          = "READER"
    domain        = var.domain
  }
}

resource "google_service_account" "bqowner" {
  account_id = local.account_id
}

resource "google_bigquery_table" "table" {
  dataset_id = google_bigquery_dataset.default.dataset_id
  table_id   = local.table_id

  external_data_configuration {
    autodetect    = true
    source_format = "GOOGLE_SHEETS"

    google_sheets_options {
      skip_leading_rows = 1
    }

    source_uris = var.table_source_uris
  }
}
