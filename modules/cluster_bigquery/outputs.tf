#############################################################
#
# DATASET OUTPUTS
#
#############################################################
output "dataset_id" {
  value = google_bigquery_dataset.dataset.id
}

output "dataset_creation_time" {
  value = google_bigquery_dataset.dataset.creation_time
}

output "dataset_etag" {
  value = google_bigquery_dataset.dataset.etag
}

output "dataset_creation_time" {
  value = google_bigquery_dataset.dataset.creation_time
}

output "dataset_last_modified_time" {
  value = google_bigquery_dataset.dataset.last_modified_time
}

output "dataset_self_link" {
  value = google_bigquery_dataset.dataset.self_link
}
