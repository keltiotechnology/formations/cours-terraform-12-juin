variable "project" {
  type        = string
  description = "The bigquery owner project"
}

variable "env" {
  type        = string
  description = "The bigquery owner env"
}

variable "id" {
  type        = string
  description = "The bigquery owner id suffix"
  default     = "dt"
}

variable "description" {
  type        = string
  description = "The bigquery metadata description"
  default     = "My super bigquery cluster description"
}

variable "location" {
  type        = string
  description = "The bigquery metadata description"
  default     = "EU"
}

variable "default_table_expiration_ms" {
  type        = string
  description = "The bigquery default table expiration ms"
  default     = null
}

variable "domain" {
  type        = string
  description = "A domain to grant access to. Any users signed in with the domain specified will be granted the specified access"
}

variable "delete_contents_on_destroy" {
  type        = string
  description = "If set to true, delete all the tables in the dataset when destroying the resource; otherwise, destroying the resource will fail if tables are present."
  default     = false
}

variable "table_source_uris" {
  type        = list(string)
  description = "A list of the fully-qualified URIs that point to your data in Google Cloud."
}
