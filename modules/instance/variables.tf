variable "machine_type" {
  type = string
}

variable "zone" {
  type        = string
  description = "La zone gcp dans laquelle les ressources seront déployées"
  default     = "us-central1-a"
}
