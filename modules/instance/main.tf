data "google_service_account" "default" {
  account_id = "id-ebo03"
}

resource "google_compute_instance" "default" {
  name         = "kevin-test"
  machine_type = var.machine_type
  zone         = var.zone

  tags = [
    "project-toto",
    "organization-titi",
    "zone-${var.zone}"
  ]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    foo = "bar"
  }

  allow_stopping_for_update = true
  metadata_startup_script   = "echo hi > /test.txt"

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = data.google_service_account.default.email
    scopes = ["cloud-platform"]
  }
}
