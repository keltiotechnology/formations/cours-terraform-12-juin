# Prise en main GCP

## Installer le cli GCP
- https://cloud.google.com/sdk/docs/install#deb

## Créer le provider dans terraform
`provider.tf`

```bash
provider "google" {
  project     = "keltio-app-1584542324136"
  region      = "europe-west1"
  zone        = "europe-west1-b"
  credentials = "${file("./secrets/keltio-auth.json")}"
}
```

## Remarques :
- Bien configurer les champs de ressources
- Utiliser infracost avant de déployer ses ressources pour avoir une estimation des coûts


# Uses cases :
# - Deployer des buckets
# - Dataproc 


# Terraform les goods practices
##################################

## Documentation
La documentation de terraform est très bien faite prennez le temps de bien lire chaque ligne pour une ressource, cela peut vous faire économiser beaucoup d'argent et de temps (https://www.terraform.io/)

## FinOps Plan de taggage :
  Utilité       :
  - Possibilité d'avoir une visibilité sur les ressources par projet / organization ... et ainsi mieux gérer la refacturation en interne et supprimer les ssources non utilisés ou surdimenssionnées

## Tfstate :
Toujours utiliser un backend lorsque l'on fait du terraform en équipe pour éviter les conflits de changement et péréniser la persistance de ce fichier si important dans terraform



# Terraform les outils
##################################
## Terragrunt :
  Utilité       : Gestion de terraform sur avec plusieurs env et version
  Site internet : https://terragrunt.gruntwork.io/
## Infracost  :
  Utilité       : Faire des architectures avec des coûts prévisible
  Site internet : https://www.infracost.io/
## Terraform documentation :
  Utilité       : Générer automatiquement la documentation pour ses modules
  Site internet : https://github.com/terraform-docs/terraform-docs
## Brainboard :
  Utilité       : Dessiner des DAT facilement et générer l'infra en code par la suite
  Site internet : https://www.brainboard.co/ (draw to code)
## modules.tf :
  Utilité       : Dessiner des DAT facilement et générer l'infra en code par la suite
  Site internet : https://modules.tf/ (draw to code)
## Bridgecrew :
  Utilité       : Scanning infra terraform et detection conformité des infras (SOC2, PCI-DSS V3.2, HIPAA, NIST-800-53, ISO27001, CIS AWS V1.2, CIS AWS V1.3)
  Site internet : https://www.bridgecrew.cloud/
## Checkov    :
  Utilité       : Donne les rapports de bridgecrew avant d'avoir commit sur git
  Site internet : https://www.checkov.io/
## Flora      :
  Utilité       : Switch facilement et rapidement de version terraform
  Site internet : https://github.com/ketchoop/flora
## Altantis   :
  Utilité       : Pull request plan validation
  Site internet : https://www.runatlantis.io/


# Type de déploiements
# #################################
# Canary deployment :
# Rolling deployment :
# Blue green deployment :


# TODO
# Se renseigner sur mega-linter
# Tfstate gestion : Recommandation

Gestion des ouvertures des flux

Contexte :
Archi      : donne les goods practices
DevOps : code le terraform infra
Sécurité : validations des ouvertures de flux
Support : doit gérer l’ouverture de flux


Solution :
un projet par infra par env
REPO 1				REPO 2
infra modulaire				réseaux security group




Identifier du refactoring qui va casser les ressources existantes

Contexte :
Service modifié par les clouds providers
Changement de la landing zone

Comment anticiper les changementes des clouds providers

Impact changement structure des données (map -> string) 


Migration smooth (canary deployment)
Choix des services et de l’infra qu’on fait



Profondeur gestion du tfstates




Check list cadrage projet :













Design thinking :

Définir les process et les règles de nommage, des acteurs …
		Acteurs :
			Lister les acteurs
			Définir pour chacun leurs responsabilités
			Vérifier si les acteurs peuvent intervenir dans l’organisation / les projets
		Convention :
			





Développeur / DevOps
Prendre connaissances de ton rôle dans l’organisation
Prendre connaissances des contraintes / standards (ligne de dev, nommages)
Voir l’existant (vérifier si un service dans l’entreprise fait déjà ce que je veux)
	Si ça existe déjà :
		Pourri : Remonte PO / PM
		Marche : Utilise
	Si y a pas :
		Remonter besoin : PM
		Notifie l’architecte pour un dev custom

Demander ou faire le DAT (dessiner l’infra, et déterminer les services à utiliser en fonction du besoin métier que)

Lire la doc terraform et determine les modules à développer
Développe les modules et les infras
Infracost (gestion des coûts)
bridgecrew (gestion de la sécu)
Tflint (Linting : Qualité du code et la maintenabilité)
terraform fmt (Linting : Qualité du code et la maintenabilité)
Pousse dans le référentiel
On test sur un ou deux env
Prod



TP plus structuré
- Etape par étape
